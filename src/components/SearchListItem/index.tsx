import React, { useState } from 'react';
import { List, Spin } from 'antd';
import styles from './index.module.css';
import {
	DownloadOutlined,
	FileOutlined,
	FolderOutlined,
} from '@ant-design/icons';
import { FileType } from '../../lib/graphql/globalTypes';
import { Search_search } from '../../lib/graphql/Search/__generated__/Search';

interface Props {
	onDownload: (
		id: string,
		type: FileType,
		setLoading: (value: boolean) => void
	) => void;
	item: Search_search;
}

export const SearchListItem = ({ onDownload, item }: Props) => {
	const [loading, setLoading] = useState<boolean>(false);

	return (
		<List.Item
			className={styles.listItem}
			actions={[
				<DownloadOutlined
					className={`${styles.icon} ${styles.downloadIcon}`}
					onClick={() => onDownload(item.id, item.type, setLoading)}
				/>,
			]}
		>
			<List.Item.Meta
				avatar={
					item.type === FileType.file ? (
						<FileOutlined className={styles.icon} />
					) : (
						<FolderOutlined className={styles.icon} />
					)
				}
				title={item.name}
			/>
			{loading ? <Spin /> : null}
		</List.Item>
	);
};
