import React from 'react';
import { Input } from 'antd';
import { SearchProps } from 'antd/lib/input';

const { Search } = Input;

interface Props extends SearchProps {}

export const SearchBar = ({
	placeholder = '',
	loading = false,
	...otherProps
}: Props) => {
	return (
		<Search
			placeholder={placeholder}
			style={{
				width: '100%',
			}}
			enterButton="Search"
			size="large"
			loading={loading}
			{...otherProps}
		/>
	);
};
