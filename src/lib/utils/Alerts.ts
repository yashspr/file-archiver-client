import { notification } from 'antd';

export const showNotification = (
	message: string,
	description: string = 'Please try again after sometime.'
) => {
	notification.open({
		message,
		description,
		duration: 0,
	});
};
