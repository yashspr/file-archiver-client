/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum ArchiverStatus {
  ALL_OK = "ALL_OK",
  FAIL = "FAIL",
}

export enum FileType {
  file = "file",
  folder = "folder",
}

export enum QueryType {
  all = "all",
  files = "files",
  folders = "folders",
}

//==============================================================
// END Enums and Input Objects
//==============================================================
