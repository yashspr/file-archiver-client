/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ArchiverStatus } from "./../../globalTypes";

// ====================================================
// GraphQL mutation operation: Archiver
// ====================================================

export interface Archiver_archiver {
  __typename: "ArchiverResult";
  status: ArchiverStatus;
}

export interface Archiver {
  archiver: Archiver_archiver | null;
}

export interface ArchiverVariables {
  rootPath: string;
}
