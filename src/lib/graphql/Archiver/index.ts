import { gql } from '@apollo/client';

export const ArchiverGQL = gql`
	mutation Archiver($rootPath: String!) {
		archiver(rootPath: $rootPath) {
			status
		}
	}
`;
