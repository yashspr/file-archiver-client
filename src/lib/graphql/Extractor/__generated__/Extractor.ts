/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FileType } from "./../../globalTypes";

// ====================================================
// GraphQL query operation: Extractor
// ====================================================

export interface Extractor_extract {
  __typename: "ExtractorResult";
  downloadLink: string;
}

export interface Extractor {
  extract: Extractor_extract;
}

export interface ExtractorVariables {
  id: string;
  type?: FileType | null;
}
