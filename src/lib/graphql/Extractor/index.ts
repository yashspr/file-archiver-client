import { gql } from '@apollo/client';

export const Extractor = gql`
	query Extractor($id: ID!, $type: FileType) {
		extract(id: $id, type: $type) {
			downloadLink
		}
	}
`;
