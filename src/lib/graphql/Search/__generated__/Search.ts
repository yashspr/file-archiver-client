/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { QueryType, FileType } from "./../../globalTypes";

// ====================================================
// GraphQL query operation: Search
// ====================================================

export interface Search_search {
  __typename: "SearchResult";
  id: string;
  name: string;
  type: FileType;
}

export interface Search {
  search: Search_search[];
}

export interface SearchVariables {
  query: string;
  type: QueryType;
}
