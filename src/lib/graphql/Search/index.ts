import { gql } from '@apollo/client';

export const SearchQuery = gql`
	query Search($query: String!, $type: QueryType!) {
		search(query: $query, type: $type) {
			id
			name
			type
		}
	}
`;
