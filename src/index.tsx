import React from 'react';
import ReactDOM from 'react-dom';
import { Home } from './pages/Home';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';

import 'antd/dist/antd.css';
import './styles/index.css';
import { Archiver } from './pages/Archiver';

import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
	uri: 'http://localhost:9000/api',
	cache: new InMemoryCache(),
});

const { Header, Footer } = Layout;

ReactDOM.render(
	<React.StrictMode>
		<ApolloProvider client={client}>
			<Router>
				<Layout className="layout">
					<Header>
						<div className="logo">File Archiver</div>
						<Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
							<Menu.Item key="1">
								<Link to="/">Home</Link>
							</Menu.Item>
							<Menu.Item key="2">
								<Link to="/archiver">Archiver</Link>
							</Menu.Item>
						</Menu>
					</Header>

					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/archiver" component={Archiver} />
					</Switch>

					<Footer style={{ textAlign: 'center' }}>File Archiver 2020</Footer>
				</Layout>
			</Router>
		</ApolloProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
