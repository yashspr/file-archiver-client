import React, { useState } from 'react';
import { Layout, Input, Spin } from 'antd';
import { FileZipOutlined } from '@ant-design/icons';
import styles from './index.module.css';
import { useApolloClient } from '@apollo/client';
import { showNotification } from '../../lib/utils/Alerts';
import {
	Archiver as ArchiverData,
	ArchiverVariables,
} from '../../lib/graphql/Archiver/__generated__/Archiver';
import { ArchiverGQL } from '../../lib/graphql/Archiver';
import { ArchiverStatus } from '../../lib/graphql/globalTypes';

const { Content } = Layout;

export const Archiver = () => {
	const client = useApolloClient();
	const [archiving, setArchiving] = useState<boolean>(false);

	const onSubmit = async (e: React.KeyboardEvent<HTMLInputElement>) => {
		console.log('onSubmit called');
		if (archiving) return;

		const rootPath = (e.target as HTMLInputElement).value;
		setArchiving(true);
		try {
			const { data } = await client.mutate<ArchiverData, ArchiverVariables>({
				mutation: ArchiverGQL,
				variables: {
					rootPath,
				},
			});

			if (data?.archiver?.status === ArchiverStatus.ALL_OK) {
				showNotification(
					'Successfully created archive',
					'Now you can safely delete the original files'
				);
			} else {
				showNotification(
					'Error creating archives',
					'Please check the log files to get more information'
				);
			}
		} catch (err) {
			console.error(err);
			showNotification(
				'Error creating archives',
				'Please check the log files to get more information'
			);
		}
		setArchiving(false);
	};

	return (
		<Layout>
			<Content className={styles.content}>
				<Input
					size="large"
					placeholder="Enter root path"
					onPressEnter={onSubmit}
					prefix={<FileZipOutlined />}
				/>
				<Spin size="large" spinning={archiving} className={styles.spinner} />
			</Content>
		</Layout>
	);
};
