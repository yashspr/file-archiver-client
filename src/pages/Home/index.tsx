import React, { useState } from 'react';
import { Layout, Select, List } from 'antd';
import styles from './index.module.scss';
import { SearchBar } from '../../components/SearchBar';
import { useApolloClient } from '@apollo/client';
import {
	Search,
	SearchVariables,
	Search_search,
} from '../../lib/graphql/Search/__generated__/Search';
import { SearchQuery } from '../../lib/graphql/Search';
import { QueryType, FileType } from '../../lib/graphql/globalTypes';
import {
	Extractor as ExtractorData,
	ExtractorVariables,
} from '../../lib/graphql/Extractor/__generated__/Extractor';
import { Extractor } from '../../lib/graphql/Extractor';
import { downloadFile } from '../../lib/utils/DOMHelpers';
import { showNotification } from '../../lib/utils/Alerts';
import { SearchListItem } from '../../components/SearchListItem';

const { Content } = Layout;
const { Option } = Select;

export const Home = () => {
	const client = useApolloClient();
	const [searchResults, setSearchResults] = useState<
		Search_search[] | null | undefined
	>(null);
	const [searchType, setSearchType] = useState<QueryType>(QueryType.files);
	const [searchLoading, setsearchLoading] = useState<boolean>(false);

	const onSearch = async (text: string) => {
		setsearchLoading(true);
		try {
			let { data } = await client.query<Search, SearchVariables>({
				query: SearchQuery,
				variables: {
					query: text,
					type: searchType,
				},
				fetchPolicy: 'network-only',
			});
			setSearchResults(data?.search);
		} catch (err) {
			console.log(err);
			showNotification('Error searching for the given terms');
		}
		setsearchLoading(false);
	};

	const onDownload = async (
		id: string,
		type: FileType,
		setLoading: (arg0: boolean) => void
	) => {
		setLoading(true);
		try {
			let { data } = await client.query<ExtractorData, ExtractorVariables>({
				query: Extractor,
				variables: {
					id,
					type,
				},
				fetchPolicy: 'network-only',
			});
			downloadFile(data?.extract.downloadLink);
			showNotification(
				'Download successful',
				'The file has been downloaded sccessfully!'
			);
		} catch (err) {
			showNotification('Unable to download the file');
			console.error(err);
		}
		setLoading(false);
	};

	const selectAfter = (
		<Select defaultValue={QueryType.files} onSelect={setSearchType}>
			<Option value={QueryType.files}>Files</Option>
			<Option value={QueryType.folders}>Folders</Option>
			<Option value={QueryType.all}>All</Option>
		</Select>
	);

	return (
		<Layout>
			<Content className={styles.content}>
				<SearchBar
					placeholder="Enter search keywords"
					onSearch={onSearch}
					addonBefore={selectAfter}
					loading={searchLoading}
				/>

				{searchResults && searchResults.length > 0 ? (
					<List
						style={{
							marginTop: '2rem',
						}}
						loading={searchLoading}
						itemLayout="horizontal"
						bordered={true}
						size="large"
						dataSource={searchResults}
						renderItem={(item) => (
							<SearchListItem
								key={item.id}
								onDownload={onDownload}
								item={item}
							/>
						)}
					/>
				) : null}
			</Content>
		</Layout>
	);
};
